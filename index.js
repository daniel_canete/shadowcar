/** @format */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import MapCom from './src/mapa/MapaComp';

AppRegistry.registerComponent(appName, () => MapCom);
