import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    Alert,
    PermissionsAndroid,
    DeviceEventEmitter

} from 'react-native';

import MapView, { Marker, Callout, AnimatedRegion, Polyline } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import KeepAwake from 'react-native-keep-awake';
import KalmanFilter from 'kalmanjs';
import Almacenamiento from '../tools/Almacenamiento';
import Utilidades from '../tools/Utilidades';
import Controles from './Controles';
import ListarSombras from './ListarSombras';
import Crono from './Crono';
import Velometro from './Velometro';
import '../tools/globals';
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog';
//import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';
import RNKalmanLocation from 'react-native-kalman-location';

import Redis from '../tools/Redis';
import Sombra from './Sombra';
import MarcaSombras from './MarcasSombras';
import ControlCarrera from './ControlCarrera';


//Solo descomentar con iOs
//import RNLocation,{ RNLocationEventEmitter } from 'react-native-location';




const markerImages = {
    marca: require('../images/marca64.png'),
    sombra: require('../images/sombra64.png'),
}

const slideAnimation = new SlideAnimation({
    slideFrom: 'bottom',
  });

const kalmanFilter = new KalmanFilter({ R: 0.01, Q: 3 });
const FILTERSIZE = 10;

const MILLISACTUALIZASOMBRA = 300;





export default class MapaComp extends Component {
    LATITUDE = 37.78825;
    LONGITUDE = -122.4324;

    state = {

        currPos: {
            latitude: this.LATITUDE,
            longitude: this.LONGITUDE,
        },
        prevPos: {
            latitude: this.LATITUDE,
            longitude: this.LONGITUDE,
        },
        miPos: new AnimatedRegion({
            latitude: this.LATITUDE,
            longitude: this.LONGITUDE,
        }),
        /*
        sombraPos: new AnimatedRegion({
            latitude: LATITUDE,
            longitude: LONGITUDE,
        }),
        */

        ruta:[],


        verControles:false,
        verControlCarrera:true,
        verListaSombras:false,
        velocidad:0,
        toogleSave:false,

        verMapa:false


    }

    region = {
        latitude: this.LATITUDE,
        longitude: this.LONGITUDE,
        latitudeDelta: 0.002,
        longitudeDelta: 0.002,
    };



    map = null;
    almacen = new Almacenamiento();

    timeRef = 0;
    lastTime = 0;


    seguirPlayer=true;


    headingData = {
        lista:[],
        tam:3,
        toleranceDegree: 30,
        lastHeading:0
    }

    veloData = {
        toleranceVelo: 400,
        lastVelo:0,
        timeToGo:2000,
        lastPosition:{longitude:0,latitude:0},
        lastMillis:0,
        countLimitVelo: 3,
        countVelo:-1

    }

    distanciaData = {
        lista:[],
        tam:5,
        toleranceDistance: 150,
        lastDistance:0,
        countLimitDistance: 3,
        countDistance:-1
    }

    posCamaraData = {
        heading:0,
        center:null,
        pitch:null

    }

    posCamaraDataTool = {
        listHeads:[],
        tamHeads:4
    }

    eventoMapaMovido=null;

    redis;
    crono;
    sombra;
    velometro;
    


    componentDidMount() {

        KeepAwake.activate();

        let date = new Date();
        this.timeRef = date.getTime();

        var self = this;

        this.redis = new Redis();
        
        this.calculaGeolocation();
        this.initCalculaVelocidad();
        this.initAnimarCamara();
        this.initSavePositionInAlmacen();
/*
        this.setState({
            verControles:true
        });
        */

        this.almacen.read(Almacenamiento.TOKEN_POSITION, function (position) {
            var _position={
                latitude:self.LATITUDE,
                longitude:self.LONGITUDE
            }
            
            if(position!=undefined && position.latitude!=undefined && position.longitude!=undefined){
                _position=position;
            }
            self.region.latitude=_position.latitude;
            self.region.longitude=_position.longitude;

            console.log(self.region);

            self.setState({
                currPos:_position,
                prevPos:_position,
                //miPos:_position,
                verControles:false,
                verControlCarrera:true,
                verMapa:true
            });
        });

        globals.cerrarListarSombras = function(){
            self.setState({
                verListaSombras:false
            });
        }

        

        
    }


    calculaMillis(){
        let millis = Utilidades.getCurrentMilis(0);


        if(this.timeRef<0){
            this.timeRef = millis;
        }

        return millis - this.timeRef;
    }

    guardaSombra(_position, millis) {
        _position.millis = millis;

        if (!globals.sombraActual) {
            globals.sombraActual = [];
        }

        globals.sombraActual.push(_position);

    }

    almacenaSombraActual() {
        var self = this;
        this.almacen.read(Almacenamiento.TOKEN_SOMBRAS, function (sombras) {
            if (!sombras) {
                sombras = [];
            }
            
            var millis = Utilidades.getCurrentMilis(0);

            //console.warn(globals.sombraActual);
            for(var i in globals.sombraActual){
                let _som = globals.sombraActual[i];
                self.redis.addLocation({
                    latitude: _som.coordenadas.latitude,
                    longitude: _som.coordenadas.longitude,
                    millis: _som.millis
                })
            }
            var _nombre = self.redis.getNombre();
            self.redis.sendSombra();
            //console.warn(millis);

            sombras.push({
                id: Utilidades.guid(),
                nombre:_nombre,
                fecha: millis,
                total: globals.sombraActual[globals.sombraActual.length-1].millis,
                datos: globals.sombraActual
            });
            self.almacen.save(Almacenamiento.TOKEN_SOMBRAS, sombras);
            globals.sombraActual=[];
            self.timeRef = -1;

            //console.warn("SAVED");
            

            
        });

    }

    initSavePositionInAlmacen(){
        var self=this;
        setInterval(function(){
            self.almacen.save(Almacenamiento.TOKEN_POSITION, self.state.currPos);
        },10000);
        
    }

    initAnimarCamara(){
        var self=this;

        setInterval(function(){
            if(!self.posCamaraData.center){
                return;
            }
            self.posCamaraDataTool.listHeads.push(self.posCamaraData.heading);
            if(self.posCamaraDataTool.listHeads.length>self.posCamaraDataTool.tamHeads){
                self.posCamaraDataTool.listHeads = self.posCamaraDataTool.listHeads.slice(1); 
            }

            var avgHead = self.posCamaraDataTool.listHeads.reduce((a, b) => a + b, 0) / self.posCamaraDataTool.listHeads.length;


            if(self.seguirPlayer){
                self.map.animateCamera({
                    heading: avgHead,
                    center: self.posCamaraData.center,
                    pitch: self.posCamaraData.pitch,
                    //altitude: 100,
                    //zoom: 20
                },
                {
                    duration: 1900
                });
            }

        },2000);
    }



    calculaVelocidad(){

        let _distancia = Utilidades.calculaGeoDistancia(this.veloData.lastPosition,this.state.currPos);

        this.veloData.lastPosition = this.state.currPos;

        let _millisActual = Utilidades.getCurrentMilis(0);
        let _difMillis = _millisActual - this.veloData.lastMillis;
        this.veloData.lastMillis = _millisActual;

        let _velocidad = Utilidades.calculaVelocidad(_distancia,_difMillis);
        if(_velocidad>this.veloData.toleranceVelo){
            return -1;
        }

        this.veloData.lastVelo = _velocidad;
        

        return Math.floor(_velocidad*100)/100;

    }

    initCalculaVelocidad(){
        var self=this;
        setInterval(function(){
            
            let _velo = self.calculaVelocidad();
            if(_velo<0) return;

            self.velometro.updateVelocidad(_velo);
            /*
            self.setState({
                velocidad:_velo
            })
            */
        },this.veloData.timeToGo);
    }

    eliminaRuidoPorDistancia(_distancia){
        this.distanciaData.countDistance++;
        if(this.distanciaData.countDistance==0){
            return false;
        }
        if(this.distanciaData.countDistance>this.distanciaData.countLimitDistance){
            this.distanciaData.countDistance=0;
            return false;
        }

        if(_distancia>this.distanciaData.toleranceDistance){
            this.distanciaData.toleranceDistance = (this.distanciaData.toleranceDistance+_distancia)/2;
            return true;
        }

        this.distanciaData.toleranceDistance = _distancia * 5;
        this.distanciaData.countDistance=0;
        return false;

        
    }

    eliminaRuidoPorVelocidad(_distancia,_difMillis){
        let _velocidadParcial= Utilidades.calculaVelocidad(_distancia,_difMillis)
        this.veloData.countVelo++;
        if(this.veloData.countVelo==0){
            return false;
        }
       // console.log("--------------------------------------");
       // console.log(_velocidadParcial+" :: "+this.veloData.countVelo);
        if(this.veloData.countVelo>this.veloData.countLimitVelo){
            this.veloData.countVelo=0;
            return false;
        }

        if(_velocidadParcial>this.veloData.toleranceVelo){
        //    console.log("--> "+_velocidadParcial);
            this.veloData.toleranceVelo = (this.veloData.toleranceVelo+_velocidadParcial)/2;
            return true;
        }

        //console.log("OK");
        this.veloData.toleranceVelo = _velocidadParcial * 3;
        this.veloData.countVelo=0;
        return false;
    }




    cuerpoLocalizacion(position){

        var self=this;

        var _latitude = position.coords.latitude;
        var _longitude = position.coords.longitude;
        var millis = this.calculaMillis();
        var _difMillis = millis-this.lastTime;
        this.lastTime=millis;

        const actual = {
            latitude: _latitude,
            longitude: _longitude
        }

        if(this.sombra){
            this.sombra.detectarBaliza(actual,millis);

            if(this.sombra.detectarInicio(actual)){
                this.crono.pausar(false);
                this.crono.reset();
                this.timeRef = Utilidades.getCurrentMilis(0);
            }

            if(this.sombra.detectarFin(actual,millis)){
                this.crono.pausar(true);
            }
        }

        let _distancia = Utilidades.calculaGeoDistancia(this.state.currPos,actual);
        
        if(this.eliminaRuidoPorDistancia(_distancia)){
            return;
        }

        if(this.eliminaRuidoPorVelocidad(_distancia,_difMillis)){
            return;
        }

        

        const curRot = Utilidades.getRotation(self.headingData,self.state.prevPos, actual);



        this.state.miPos.timing({
            latitude:actual.latitude,
            longitude:actual.longitude,
            duration:_difMillis
        }).start();
        

        var _pitch=45;
        if(Platform.OS=="android"){
            _pitch=75;
        }


        if(self.seguirPlayer){

            var _center = Utilidades.calculaOffsetPosition(actual,100,curRot);
            self.posCamaraData={
                heading: curRot,
                center: _center,
                pitch: _pitch
            };

        }

        if(self.state.toogleSave){
            self.guardaSombra({
                coordenadas: actual,
                rotacion: curRot
            },millis);
        }

        self.state.prevPos=self.state.currPos;
        self.state.currPos=actual;
        /*
        self.setState({
            //region:_reg,
            prevPos: self.state.currPos,
            currPos: actual
        });
        */

    }

    localizar(){
        this.localizarDebug(-80);
        return;
        
        if(Platform.OS=="ios"){
            this.localizarIOS();
        }else{
            this.localizarAndroid3();
        }
    }


    localizarDebug(_phase){
        var self=this;
        var lastMillis=0;

        var initLocalizacionDebug = (_locs,idx) =>{
            var self=this;
            let localizacion = _locs[idx];
            
            if(localizacion){
                let _time = (localizacion.millis - lastMillis)+_phase;
                setTimeout(function(){
                    //console.warn(localizacion);
                    self.cuerpoLocalizacion({
                        coords:{
                            longitude:localizacion.longitude,
                            latitude:localizacion.latitude
                        }
                    });
                    initLocalizacionDebug(_locs,idx+1)
                },_time);
                lastMillis = localizacion.millis;
            }
        }

        //S20190101-13-nqAfAdaXgKCOkeLc : andando alrededor de casa
        //S20190101-13-JtSQGLnwiElkgeOO : en coche alrededor de casa
        //S20190101-09-tXFMinyWUfMrYiTv : Apple en coche
        //S20190102-10-nzoypLsgoNmplodq : Coche SE-20 Vuelta
        //S20190102-10-qvNCwWfpvBuvQPqG : Coche Retorno casa desde parque aviones
        //S20190102-08-rWayrSAlalVhwIZo : Coche ida por SE-20

        //S20190103-12-maXsRshjxpFSyrQm : Pista direccion madrid 1
        //S20190103-09-MhOGHnFZolIuvtie : Pista direccion madrid 2
        //S20190103-09-hWWuekLxsRdywaRW : Pista direccion madrid 3
        //S20190103-19-RhmcbFzfMrujGxWt : Kansas city Direccion cordoba sur
        //S20190103-18-lHXUGFJDHdXRbOoD : Cordoba sur direccion padres

        //S20190104-19-kniGLVwktWGhmnEl : Domino's hasta casa

        this.redis.getSombra('S20190103-09-hWWuekLxsRdywaRW',function(data){
            initLocalizacionDebug(data.locations,0)
        });
    }

    localizarAndroid3(){
        var self=this;
        this.evEmitter = DeviceEventEmitter.addListener('kalmanFilter', (location) => {
            self.cuerpoLocalizacion({
                coords:location
            });
        });

        setTimeout(function(){
          RNKalmanLocation.getLocation();
        },2000);
    }


    componentWillUnmount() {
        //BackgroundGeolocation.stop();
        // unregister all event listeners
        //BackgroundGeolocation.events.forEach(event => BackgroundGeolocation.removeAllListeners(event));

        this.evEmitter.remove();
		//	Stop emiting location, this is required!!
		RNKalmanLocation.removeListener();
    }

    

    localizarIOS() {
        var self = this;

        //var RNLocation = require("react-native-location");
        //var RNLocationEventEmitter = require("react-native-location");

        RNLocation.requestAlwaysAuthorization();
        RNLocation.startUpdatingLocation();
        RNLocation.setDistanceFilter(5.0);
        //RNLocation.startUpdatingHeading();

        var subscription = RNLocationEventEmitter.addListener(
            'locationUpdated',
            (position) => {
                self.cuerpoLocalizacion(position);

            }
        );

  


    }
    


    calculaGeolocation() {
        var self = this;


        if (Platform.OS == 'ios') {
            Geolocation.requestAuthorization();
            self.localizar();
        } else {
            PermissionsAndroid.request(
                //PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Permitir obtener tu localizacion',
                    'message': '¿Aceptas que esta aplicación pueda obtener tu localización?.'
                }
            ).then(function (_grant) {
                
                self.localizar();
            });

        }
    }

    igualaSombraAActual(){
        globals.igualarSombra(this.state.currPos);
    }


    

    onGuardarSombra(){
        if(this.state.toogleSave){
            //Finalizando
            this.crono.pausar(true);
            this.almacenaSombraActual();
        }else{
            //Empezando
            globals.sombraActual=[];
            this.timeRef = Utilidades.getCurrentMilis(0);
            this.crono.pausar(false);
            this.crono.reset();
        }
        this.setState({
            toogleSave:!this.state.toogleSave
        });
    }

    onListarSombras(){
        this.setState({
            verListaSombras:true
        });
    }

    resetCrono(){
        this.crono.reset();
    }

    mueveMapa(){
        if(this.eventoMapaMovido){
            clearTimeout(this.eventoMapaMovido);
        }
        this.seguirPlayer=false;
        var self=this;
        this.eventoMapaMovido = setTimeout(function(){
            self.seguirPlayer=true;
        },10000);

    }

    closeSombra(){
        console.warn('close');
    }

    

    render() {

        if(!this.state.verMapa){
            return(<View/>);
        }

        return (
            <View style={styles.container}>

                <View
                    style={styles.mapView}
                >
                    <MapView
                        ref={map => this.map = map}
                        moveOnMarkerPress={false}

                        initialRegion={this.region}

                        scrollEnabled={true}
                        style={styles.mapContainer}
                        rotateEnabled={true}
                        onPanDrag={this.mueveMapa.bind(this)}
                    >
                        <Marker.Animated
                            ref={marker => { this.marker = marker; }}
                            image={markerImages['marca']}
                            coordinate={this.state.miPos}
                        />
                        
                        <Sombra 
                            ref={sombra => { this.sombra = sombra; }}
                            icon={markerImages['sombra']}
                        />

                        <MarcaSombras />

                        

                    </MapView>

                    <ControlCarrera 
                        renderizar={this.state.verControlCarrera}
                        onClose={this.closeSombra.bind(this)}
                    />

                    <Crono 
                        ref={crono => this.crono = crono}
                        toogleSave = {this.state.toogleSave}
                    />

                    <Velometro 
                        ref={velometro => this.velometro = velometro}
                        velocidad={this.state.velocidad}
                    />

                    <Controles 
                            renderizar={this.state.verControles}
                            onRefreshSombra = {this.igualaSombraAActual.bind(this)}
                            onGuardarSombra = {this.onGuardarSombra.bind(this)}
                            onListarSombras = {this.onListarSombras.bind(this)}
                        />


                </View>

                <PopupDialog
                    visible={this.state.verListaSombras}
                    dialogAnimation={slideAnimation}
                    dialogStyle={styles.dialog}
                    >
                    <ListarSombras/>
                </PopupDialog>



            </View>




        );
    }


}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    mapView: {
        flex: 1,
    },
    mapContainer: {
        flex: 1
    },
    dialog:{
        flex:0.7
    },
});