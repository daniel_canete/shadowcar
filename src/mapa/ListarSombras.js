import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    ScrollView,
    View,
    Dimensions,
    TouchableHighlight

} from 'react-native';


import Utilidades from '../tools/Utilidades';
import moment from 'moment';

export default class ListarSombras extends Component {

    state={
        listadoSombras:[]
    }

    seleccionaSombra(sombra){
        globals.setSombra(sombra);
        globals.cerrarListarSombras();
    }

    

   imprimeSombras(){
       var self=this;
        Utilidades.readSombras(function(sombras){
            var lista=[];
            for(var i in sombras){
                let sombra = sombras[i];

                
                let _nombre = sombra.nombre;
                let _fec = moment(sombra.fecha).utc().format('YYYY/MM/DD HH:mm');
                
                let _total = 0;


                if(sombra.datos){
                   _total=sombra.datos.length;
                }

                lista.push(
                    <TouchableHighlight
                        key={sombra.id}
                        style={styles.botones}
                        onPress={self.seleccionaSombra.bind(self,sombra)}
                    >
                        <Text style={styles.botonesTX}>{_fec} {_nombre} {_total}</Text>
                    </TouchableHighlight>
                );
            }

            

            
            self.setState({
                listadoSombras:lista
            });
        })
   }


    cerrar(){
        globals.cerrarListarSombras();
    }

    componentDidMount(){
        this.imprimeSombras();
    }


    render(){
        return(

            <ScrollView style={styles.container}>
                <TouchableHighlight
                        style={styles.BTClose}
                        underlayColor="rgba(0,0,0,0.6)"
                        onPress={this.cerrar.bind(this)}
                    >
                        <Text style={styles.botonesTX}>CANCELAR</Text>
                </TouchableHighlight>
                {this.state.listadoSombras}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:0.7,
        padding:10,
        borderRadius:10,
        backgroundColor:"#fff",
        width:Dimensions.get('window').width-20
    },
    BTClose:{
        backgroundColor:"#999",
        padding:10,
        borderRadius:10
        
    },
    botones:{
        backgroundColor:"#333",
        padding:10,
        margin:5,
        borderRadius:10
    },
    botonesTX:{
        color:"#fff",
        fontSize:14
    }
});