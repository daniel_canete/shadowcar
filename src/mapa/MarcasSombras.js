import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Alert

} from 'react-native';

import { Marker } from 'react-native-maps';


const markerImages = {
    salida: require('../images/salida125.png'),
    puntogris: require('../images/puntogris.png'),
}
import Utilidades from '../tools/Utilidades';
import moment from 'moment';

export default class MarcasSombras extends Component {

    state={
        sombras:[] 
    }

    constructor(props){
        super(props);

        
    }

    componentDidMount(){
        var self=this;
        Utilidades.readSombras(function(sombras){
            self.setState({
                sombras:sombras
            })

        });
    }

    setSombra(indice){
        globals.setSombra(this.state.sombras[indice]);
    }



    render(){

        var marcas=[];

        for(var i in this.state.sombras){
            let _som=this.state.sombras[i];
            if(!_som.datos || !_som.total ||_som.datos.length==0){
                continue;
            }

            let _coordenadas = _som.datos[0].coordenadas;
            let tiempo =  moment(_som.total).utc().format('mm:ss');
            let _key = "MarcaSombra_"+i;
            let indice=i;

            

/*
            marcas.push(
                <Marker
                    key={_key}
                    coordinate={_coordenadas}
                    onPress={this.setSombra.bind(this,indice)}
                >
                    <View style={[styles.viewMarca]}>
                        <Text style={[styles.textoMarca]}>
                        {tiempo}</Text>
                    </View>
                </Marker>
            );
            */

            marcas.push(
                <Marker
                    key={_key}
                    coordinate={_coordenadas}
                    image={markerImages['puntogris']}
                    onPress={this.setSombra.bind(this,indice)}
                >
                </Marker>
            );
            
        }

        

        return (marcas);



    }

}

const styles = StyleSheet.create({
    viewMarca:{
        marginLeft:30,
        backgroundColor:'#000b',
        borderRadius:5
    },
    textoMarca:{
        fontFamily: Platform.OS === 'ios' ? "Digital-7 Mono" : "Digital-7",
        fontSize:15,
        padding:5,
        color:"#fff"
    }

});