import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableHighlight

} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

export default class Controles extends Component {

    state={
        colorSeguir:"#000",
        seguir:true,
        verControles:true
    }

    constructor(props){
        super(props);

        this.props = props;
    }

    componentWillReceiveProps(nprop) {
        this.setState({
          verControles:nprop.renderizar
        });
      }
    

    refreshSombraActual() {
        if(!this.props.onRefreshSombra){
            return;
        }

        this.props.onRefreshSombra();
    }

    guardarSombraActual() {
        if(!this.props.onGuardarSombra){
            return;
        }
        
        this.props.onGuardarSombra();
    }

    listarSombras() {
        if(!this.props.onListarSombras){
            return;
        }
        
        this.props.onListarSombras();
    }

    seguirSombraActual() {
        let seguir=!this.state.seguir;
        let colorSeguir="#000";
        if(!seguir){
            colorSeguir="#0007";
        }

        this.setState({
            seguir:seguir,
            colorSeguir:colorSeguir
        });

        if(!this.props.onSeguirSombra){
            return;
        }
        
        this.props.onSeguirSombra(seguir);
    }

    render() {
        if(!this.props.renderizar){
            return (<View/>);
        }

        return (
            <View style={styles.container}>
                <TouchableHighlight style={styles.botones}

                    underlayColor="rgba(0,0,0,0.6)"

                    onPress={this.refreshSombraActual.bind(this)}>
                    <Icon
                        name="refresh"
                        size={40}
                        color="#000"
                    />


                </TouchableHighlight>

                <TouchableHighlight style={styles.botones}

                    underlayColor="rgba(0,0,0,0.6)"

                    onPress={this.seguirSombraActual.bind(this)}>
                    <Icon
                        name="chevron-circle-right"
                        size={40}
                        color={this.state.colorSeguir}
                    />


                </TouchableHighlight>

                <TouchableHighlight style={styles.botones}

                    underlayColor="rgba(0,0,0,0.6)"

                    onPress={this.guardarSombraActual.bind(this)}>
                    <Icon
                        name="save"
                        size={40}
                        color="#000"
                    />


                </TouchableHighlight>

                <TouchableHighlight style={styles.botones}

                    underlayColor="rgba(0,0,0,0.6)"

                    onPress={this.listarSombras.bind(this)}>
                    <Icon
                        name="list-ul"
                        size={40}
                        color="#000"
                    />
                </TouchableHighlight>

            </View>


        );
    }

}

const styles = StyleSheet.create({
    container: {
        position:'absolute',
        flexDirection:'row',
        marginTop: Dimensions.get('window').height - 100,
        marginBottom: 50,
        height: 100,
        width:Dimensions.get('window').width,
        justifyContent:'center'
    },
    botones: {
        padding: 10,
        backgroundColor: "#0004",
        height:80,
        width:60,
        borderRadius: 10,
        marginLeft: 10,
        marginRight: 10,
        justifyContent:'center',
        flexDirection:'row'
    }
});