import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableHighlight

} from 'react-native';



export default class Velometro extends Component {

    state={
        velocidad:0
    }



    render(){
        return(
            <View style = {styles.container}>
                <Text style = {styles.textoVelo}>{this.state.velocidad} KH</Text>
            </View>
        )
    }

    updateVelocidad(_velo){
        this.setState({
            velocidad:_velo
        })
    }

    

    componentDidMount(){

    }

    constructor(props){
        super(props);
        
    }

}

const styles = StyleSheet.create({
    container:{
        position:'absolute',
        backgroundColor:'#999',
        right:10,
        top:75,
        margin:10,
        borderRadius:5,
        padding:5
    },
    textoVelo:{
        fontFamily: Platform.OS === 'ios' ? "Digital-7 Mono" : "Digital-7",
        fontSize:28,
        color:'#fff',
    }
});