import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { Marker,AnimatedRegion,Polyline,Callout} from 'react-native-maps';
import Utilidades from '../tools/Utilidades';
import moment from 'moment';
import Hablar from '../tools/Hablar';
import Traductor from '../tools/Traductor';


const markerImages = {
    sombra: require('../images/sombra64.png'),
    llegada: require('../images/llegada125.png'),
    salida: require('../images/salida125.png'),
    intermedio: require('../images/intermedio100.png'),
    puntogris: require('../images/puntogris.png'),
    puntorayas: require('../images/puntorayas.png'),
    puntorojo: require('../images/puntorojo.png'),
    puntoazul: require('../images/puntoazul.png'),
    puntoamarillo: require('../images/puntoamarillo.png'),
    puntoverde: require('../images/puntoverde.png'),
}

const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const BALIZA_DISTANCIA = 10;
const BALIZA_NUM = 3;

export default class Sombra extends Component {

    state={
        sombraPos: new AnimatedRegion({
            latitude: LATITUDE,
            longitude: LONGITUDE,
        }),
        ruta:[],
        intermedios:[],
        llegada:{
            titulo:"00:00",
            color:"#000b"
        }
    }

    headingData = {
        lista:[],
        tam:3,
        toleranceDegree: 15,
        lastHeading:0
    }

    posicionesSombra = [];

    inciado=false;
    finalizado=false;

    
    total=0;

    eventActualizacionSombra=null;

    constructor(props){
        super(props);

        var self=this;

        
        globals.setSombra = function(sombra){
            if(!sombra.datos){
                return;
            }

            self.posicionesSombra = sombra.datos;
            if(self.posicionesSombra.length==0){
                return;
            }
            self.total = self.posicionesSombra[self.posicionesSombra.length-1].millis-self.posicionesSombra[0].millis;
            self.state.llegada.titulo = moment(self.total).utc().format('mm:ss');
            self.setRutaSombra(0);
            self.inciado=false;
            self.finalizado=false;
            self.actualizarPosicionSombra(0);
            
        }

        globals.igualarSombra = function(posActual){
            self.igualaSombraAActual(posActual);
        }
        

    }

    setIntermedios(){
        this.state.intermedios=[];
        var _partesMillis=this.total/(BALIZA_NUM+1);
        var _intMillis=_partesMillis;
        for(var i in this.posicionesSombra){
            let _som=this.posicionesSombra[i];
            if(_som.millis>_intMillis){
                var _timeTit=moment(_som.millis).utc().format('mm:ss');
                this.state.intermedios.push({
                    latitude:_som.coordenadas.latitude,
                    longitude:_som.coordenadas.longitude,
                    titulo:_timeTit,
                    baliza:false,
                    positivo:true,
                    millis:_som.millis,
                    color:"#000b"
                });
                _intMillis+=_partesMillis;
            }

            if(this.state.intermedios.length==BALIZA_NUM){
                return;
            }
        }
    }

    detectarInicio(posActual){
        if(this.inciado){
            return false;
        }

        if(this.posicionesSombra.length==0){
            return false;
        }

        let _dist = Utilidades.calculaGeoDistancia(posActual,this.posicionesSombra[0].coordenadas);
        if(_dist<BALIZA_DISTANCIA){
            this.inciado=true;
            this.actualizarPosicionSombra(0);
            Hablar.frase(Traductor.traducir("ComienzaCarrera"));
            return true;
        }
        
    }

    detectarFin(posActual,millisActual){
        if(!this.inciado){
            return false;
        }

        if(this.finalizado){
            return false;
        }

        if(this.posicionesSombra.length==0){
            return false;
        }

        let _dist = Utilidades.calculaGeoDistancia(posActual,this.posicionesSombra[this.posicionesSombra.length-1].coordenadas);
        if(_dist<BALIZA_DISTANCIA){
            this.finalizado=true;

            let _color;
            let _timeTit;
            let _signo="";
            let _diffMillis=0;
            let _hablaMin;
            let _hablaSec;
            let _hablaMil;
            let _vasPor;

            if(millisActual<this.total){
                _color="#060c";
                _signo="-";
                _diffMillis=this.total-millisActual;
                _vasPor=Traductor.traducir("Hasganado");
            }else{
                _color="#600c";
                _signo="+";
                _diffMillis=millisActual-this.total;
                _vasPor=Traductor.traducir("Hasperdido");
            }

            _timeTit=_signo+moment(_diffMillis).utc().format('mm:ss.SSS');
            _hablaMin=moment(_diffMillis).utc().format('m')+" "+Traductor.traducir("minutos")+", ";
            _hablaSec=moment(_diffMillis).utc().format('s')+" "+Traductor.traducir("segundos")+" y ";
            _hablaMil=moment(_diffMillis).utc().format('SSS')+" "+Traductor.traducir("milesimas");

            let _frase = _vasPor+". "+_hablaMin+" "+_hablaSec+" "+_hablaMil;
            console.log(_frase);
            Hablar.frase(_frase);


            this.setState({
                llegada:{
                    color:_color,
                    titulo:_timeTit
                }
            })


            return true;
        }
        
    }

    detectarBaliza(posActual,millisActual){
        if(!this.inciado){
            return false;
        }
        
        if(this.finalizado){
            return false;
        }

        var _intermedios=JSON.parse(JSON.stringify(this.state.intermedios));
        for(var i in _intermedios){
            let _interm = _intermedios[i];
            if(_interm.baliza){
                continue;
            }

            let _dist = Utilidades.calculaGeoDistancia(posActual,_interm);
            if(_dist<BALIZA_DISTANCIA){
                _intermedios[i].baliza=true;
                _intermedios[i].positivo=true;
                let _signo="";
                let _diffMillis=0;

                let _timeTit;
                let _hablaMin;
                let _hablaSec;
                let _hablaMil;
                let _vasPor;

                if(millisActual<_interm.millis){
                    _intermedios[i].color="#060c";
                    _signo="-";
                    _diffMillis=_interm.millis-millisActual;
                    _vasPor=Traductor.traducir("Vasdelante");
                    _intermedios[i].positivo=false;
                    
                }else{
                    _intermedios[i].color="#600c";
                    _signo="+";
                    _diffMillis=millisActual-_interm.millis;
                    _vasPor=Traductor.traducir("Vasdetras");
                    _intermedios[i].positivo=true;
                }

                _timeTit=_signo+moment(_diffMillis).utc().format('mm:ss.SSS');
                _hablaMin=moment(_diffMillis).utc().format('m')+" "+Traductor.traducir("minutos")+", ";
                _hablaSec=moment(_diffMillis).utc().format('s')+" "+Traductor.traducir("segundos")+" y ";
                _hablaMil=moment(_diffMillis).utc().format('SSS')+" "+Traductor.traducir("milesimas");

                let _frase = _vasPor+". "+_hablaMin+" "+_hablaSec+" "+_hablaMil;
                console.log(_frase);
                Hablar.frase(_frase);

                _intermedios[i].titulo=_timeTit;
                
                this.setState({
                    intermedios:_intermedios
                })
            }
        }

    }

    

    setRutaSombra(_idx){
        this.setState({
            ruta:[]
        });

        var self=this;
        var ultimaSombra=null;
        var lastRot=0;

        this.setIntermedios();


        setTimeout(function(){
            var ruta = [];
            var _posicionesSombra = JSON.parse(JSON.stringify(self.posicionesSombra));
            for(var i in _posicionesSombra){
                var _som = _posicionesSombra[i].coordenadas;
                var _somRut = _posicionesSombra[i].coordenadas;
                _som.millis = _posicionesSombra[i].millis;
                
                if(i>=_idx){
                    if(!ultimaSombra){
                        ultimaSombra = _som;
                        ruta.push(_somRut);
                        continue;
                    } 
                    let curRot = Utilidades.getRotation(self.headingData,ultimaSombra, _som);
                    let diff = curRot-lastRot;
                    if(Math.abs(diff)<self.headingData.toleranceDegree){
                        lastRot += curRot/10;
                        continue;
                    }
                    lastRot=curRot;

                    ruta.push(_somRut);
                }   
            }

            self.setState({
                ruta
            });
        },1000);
    }

    igualaSombraAActual(posActual){
        var min = -1;
        var posMin= 0;
        var _posiciones = this.posicionesSombra;
       // console.warn(_posiciones.length);
        for(var i in _posiciones){
            var _sombra = _posiciones[i];
            var _distancia = Utilidades.calculaGeoDistancia(posActual,_sombra.coordenadas);
            if(min==-1 || _distancia<min){
                min = _distancia;
                posMin = i;

            }
        }


        this.posicionesSombra = this.posicionesSombra.slice(posMin);
        this.setRutaSombra(0);
        this.inciado=false;
        this.finalizado=false;
        this.actualizarPosicionSombra(0);
        

    }

    actualizarPosicionSombra(posIni){

        var _posIni = posIni;
        var _timeRef=0;
        
        var self=this;
            if(!self.posicionesSombra[_posIni]){
                //No hay mas datos
                return;
            }


            var lastMillis = self.posicionesSombra[_posIni].millis;
            

            if(self.eventActualizacionSombra){
                clearInterval(self.eventActualizacionSombra);
            }
        
    
            var initLocalizacionSombra = (_locs,idx) =>{
                var self=this;
                let localizacion = _locs[idx];
                if(!localizacion){
                    //Ya no hay mas datos
                    return;
                }

                idx++;
                if(localizacion){
                    let _time = (localizacion.millis - lastMillis) ;
                    let _desfase = Utilidades.getCurrentMilis(0)-_timeRef-localizacion.millis;
                    //console.log(_desfase);
                    _time-=_desfase;
                    self.eventActualizacionSombra = setTimeout(function(){
                        //console.warn(localizacion);
                        
                        let actual=localizacion.coordenadas;
                        self.state.sombraPos.timing({
                            latitude:actual.latitude,
                            longitude:actual.longitude,
                            duration:_time
                        }).start();

                        /*
                        if(self.seguirSombra){
                            self.posCamaraData={
                                heading: localizacion.rotacion,
                                center: localizacion.coordenadas,
                                pitch: 45
                            };
                        }
                        */

                        initLocalizacionSombra(_locs,idx);

                        
                    },_time);
                    lastMillis = localizacion.millis;
                }
            }
    
            if(this.inciado){
                initLocalizacionSombra(self.posicionesSombra,_posIni);
                _timeRef = Utilidades.getCurrentMilis(0);
            }
  
    }


    render(){
        var datos = [
            <Marker.Animated
                key="sombraMarker"
                image={this.props.icon}
                coordinate={this.state.sombraPos}
            />
            ,
            <Polyline
                key="sombraRuta"
                            coordinates={this.state.ruta}
                            strokeWidth={15}
                            strokeColor="#00f3"
            />
        ];

        if(this.state.ruta.length>0){

            /*
            datos.push(
                <Marker
                    key="sombraLlegada"
                    coordinate={this.state.ruta[this.state.ruta.length-1]}
                    
                >
                    <View style={[styles.viewLlegada,{backgroundColor:this.state.llegada.color}]}>
                        <Text style={styles.textoLLegada}>
                            {this.state.llegada.titulo}</Text>
                    </View>
                </Marker>
            );
            */

            datos.push(
                <Marker
                    key="sombraLlegada"
                    coordinate={this.state.ruta[this.state.ruta.length-1]}
                    image={markerImages['puntoazul']}
                    
                >
                </Marker>
            );

            /*
            datos.push(
                <Marker
                    key="sombraSalida"
                    coordinate={this.state.ruta[0]}
                    
                />
            );
            */

            
            for(var i in this.state.intermedios){
                let _intermedio=this.state.intermedios[i];
                let _key="sombraIntermedio"+i;
                let _punto='puntoazul';
                if(_intermedio.baliza){
                    if(_intermedio.positivo){
                        _punto='puntorojo';
                    }else{
                        _punto='puntoverde';
                    }
                }
                datos.push(
                    <Marker
                        key={_key}
                        coordinate={_intermedio}
                        image={markerImages[_punto]}
                    >
                    </Marker>
                );

                /*
                datos.push(
                    <Marker
                        key={_key}
                        coordinate={_intermedio}
                        image={markerImages['puntoazul']}
                    >
                        <View style={[styles.viewIntermedio,{backgroundColor:_intermedio.color}]}>
                            <Text style={[styles.textoIntermedio]}>
                            {_intermedio.titulo}</Text>
                        </View>
                    </Marker>
                );
                */
            }
        }


        return datos;
    }

}

const styles = StyleSheet.create({
    viewIntermedio:{
        marginBottom:180,
        backgroundColor:'#000b',
        borderRadius:5
    },
    textoIntermedio:{
        fontFamily: Platform.OS === 'ios' ? "Digital-7 Mono" : "Digital-7",
        fontSize:20,
        padding:5,
        color:"#fff"
    },
    viewLlegada:{
        borderRadius:5
    },
    textoLLegada:{
        fontFamily: Platform.OS === 'ios' ? "Digital-7 Mono" : "Digital-7",
        fontSize:25,
        color:'#fff',
        padding:5,
    }
});