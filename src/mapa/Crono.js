import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableHighlight

} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import moment from 'moment';
import Utilidades from '../tools/Utilidades';

export default class Crono extends Component {

    state={
        crono:"00:00:000"
    }

    milis=0;
    initMilis=0;
    pause=true;


    render(){
        var _color="#fff";
        if(this.props.toogleSave){
            _color="#800";
        }
        return(
            <View style = {styles.container}>
                <Text style = {[styles.textoCrono,{color:_color}]}>{this.state.crono}</Text>
            </View>
        )
    }

    getWorldMilis(){
        var d = new Date();
        return d.getTime();
    }

    constructor(props){
        super(props);
    }

    reset(){
        this.initMilis = this.getWorldMilis();
    }

    pausar(_val){
        this.pause = _val;
    }

    

    initCrono(){
        
        var self = this;
        setInterval(function(){
            if(self.pause){
                return;
            }
            
            this.milis=Utilidades.getCurrentMilis(self.initMilis);
            self.setState({
                crono : moment(milis).utc().format('HH:mm:ss.SSS')
            });
        },300);
    }

    componentDidMount(){
        this.initMilis = this.getWorldMilis();

        this.initCrono();
    }

}

const styles = StyleSheet.create({
    container:{
        position:'absolute',
        backgroundColor:'#999',
        right:10,
        top:25,
        margin:10,
        borderRadius:5,
        padding:5
    },
    textoCrono:{
        fontFamily: Platform.OS === 'ios' ? "Digital-7 Mono" : "Digital-7",
        fontSize:28,
        color:'#fff',
    }
});