import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableHighlight

} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import moment from 'moment';
import Utilidades from '../tools/Utilidades';

export default class ControlCarrera extends Component {

    state={
        crono:"00:00:000"
    }

    milis=0;
    initMilis=0;
    pause=true;


    render(){
        if(!this.props.renderizar){
            return (<View />);
        }
        var _color="#fff";
        if(this.props.toogleSave){
            _color="#800";
        }
        return(
            <View style = {styles.container}>
                <View style = {styles.textoCrono}>
                    <Text style = {[styles.textoCronoTX,{color:_color}]}>{this.state.crono}</Text>
                </View>
                
                <TouchableHighlight 
                    style = {styles.closeSombra}
                    onPress = {this.props.onClose}>
                    <Text style = {styles.closeSombraTX}>X</Text>
                </TouchableHighlight>
            </View>
        )
    }

    getWorldMilis(){
        var d = new Date();
        return d.getTime();
    }

    constructor(props){
        super(props);
    }


    initCrono(){
        
        var self = this;
        setInterval(function(){
            if(self.pause){
                return;
            }
            
            this.milis=Utilidades.getCurrentMilis(self.initMilis);
            self.setState({
                crono : moment(milis).utc().format('HH:mm:ss.SSS')
            });
        },300);
    }

    componentDidMount(){
        this.initMilis = this.getWorldMilis();

        this.initCrono();
    }

}

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#999',
        bottom:10,
        margin:10,
        borderRadius:5,
        padding:5,
        flexDirection:'row'

    },
    textoCrono:{
        flex:0.9
    },
    textoCronoTX:{
        fontFamily: Platform.OS === 'ios' ? "Digital-7 Mono" : "Digital-7",
        fontSize:28,
        color:'#fff',
        padding:5
    },
    closeSombra:{
        flex:0.1
    },
    closeSombraTX:{
        color:'#fff',
        fontSize:28,
        width:28,
        padding:5,
        borderRadius:5,
        fontFamily: Platform.OS === 'ios' ? "Digital-7 Mono" : "Digital-7",
        backgroundColor:"#770000",
        
    }
});