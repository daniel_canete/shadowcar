
var React,{AsyncStorage} = require("react-native");
import './globals.js';

class Almacenamiento{

	static TOKEN_SOMBRAS="token_sombras_k";
	static TOKEN_POSITION="token_position_k";

	st_claves='__claves';


	constructor(){

	}

	carga_inicial(){
		this.read(this.st_claves,(lista)=>{
			
			if((typeof lista.resultado) != 'string'){
				return;
			}

			var _lista=lista.resultado.split(",");
			
			for(var _c in _lista){
				AsyncStorage.getItem(_lista[_c]).then((valor) => {
					globals.valores[_lista[_c]]=valor;
				}).done();
			}
		});
	}

	read(clave,Callback){
		AsyncStorage.getItem(clave).then((valor) => {
			try{
				if(globals && globals.valores){
					globals.valores[clave]=valor;
				}
				Callback(JSON.parse(valor),false);
			}catch(excep){
				console.log(excep);
				Callback(false,excep);
			}
			
			
	    }).done();
	}

	save(clave,valor){
		try{
			AsyncStorage.setItem(clave, JSON.stringify(valor));

			if(globals && globals.valores && globals.claves){
				globals.valores[clave]=valor;
				globals.claves.push(clave);
				AsyncStorage.setItem(this.st_claves, globals.claves.join());
			}
		}catch(excep){
			console.log(excep);
		}
		

	}

	debug(){
		console.log("CLAVES: ");
		for(var i in global.valores){
			console.log(i+" -> "+global.valores[i]);
		}
	}
}

export default Almacenamiento;