import Almacenamiento from '../tools/Almacenamiento';

export default class Utilidades {

    static guid = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    static calculaGeoDistancia(origen,destino) {
        let lat1=origen.latitude;
        let lat2=destino.latitude;
        let lon1=origen.longitude;
        let lon2=destino.longitude;

        rad = function (x) { return x * Math.PI / 180; }
        var R = 6378.137; //Radio de la tierra en km
        var dLat = rad(lat2 - lat1);
        var dLong = rad(lon2 - lon1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) 
          * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return (d * 1000).toFixed(3);
    }

    static calculaVelocidad(distancia,millis) {
        if(millis==0) return 0;


        let _dist = ((distancia * (60*60) )/ millis) ;

        return _dist;
        
    }

    static getCurrentMilis(_init){
        var d = new Date();
        return d.getTime() - _init;
    }

    static calculaOffsetPosition(origen,distance,angulo) {
        //Position, decimal degrees
        var lat = origen.latitude;
        var lon = origen.longitude;

        //Earth’s radius, sphere
        var R=6378137;


        //Coordinate offsets in radians
        var distLat = distance/R
        var distLon = distance/(R*Math.cos(Math.PI*lon/180))

        


        //OffsetPosition, decimal degrees
        //var latO = lat + dLat * 180/Math.PI;
        //var lonO = lon + dLon * 180/Math.PI;
        var latO = lat + distLat * Math.sin(angulo);
        var lonO = lon + distLon * Math.cos(angulo);

        return ({
            latitude:latO,
            longitude:lonO
        })
    }

    static randomString(length, chars) {
        var mask = '';
        if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
        if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if (chars.indexOf('#') > -1) mask += '0123456789';
        if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
        var result = '';
        for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
        return result;
    }

    static getRotation(headingData,prevPos, curPos) {
        
        
        if (!prevPos) return 0;
        const xDiff = curPos.latitude - prevPos.latitude;
        const yDiff = curPos.longitude - prevPos.longitude;
        var _heading = (Math.atan2(yDiff, xDiff) * 180.0) / Math.PI;

        headingData.lista.push(_heading);
        if(headingData.lista.length > headingData.tam){
            headingData.lista = headingData.lista.slice(1);
        }
        
        var avg = headingData.lista.reduce((a, b) => a + b, 0) / headingData.lista.length;
        var diff = Math.abs(avg-_heading);


        if( diff > headingData.toleranceDegree ){
            return headingData.lastHeading;
        }

        headingData.lastHeading = _heading;
        return _heading;
        
    }


    static readSombras(_callback){
        var lista = [];
        let almacen=new Almacenamiento();

        almacen.read(Almacenamiento.TOKEN_SOMBRAS, function (sombras) {

            
            if (!sombras || sombras.length==0) {
                _callback([]);
                return;
            }

            sombras.sort(function(a, b) {
                return b.fecha - a.fecha;
            });

            try{
            
                for(var i in sombras){
                    let sombra = sombras[i];
                    lista.push(sombra);

                    
                }

                
               _callback(lista);
                
            }catch(ex){
                //console.warn(ex);
                _callback([]);
            }
        });

        

    }
}