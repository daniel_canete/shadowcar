export default class Traductor{
    language="ES";

    traducciones={
        "ES":{
            "ComienzaCarrera":"¡Comienza la carrera!",
            "minutos":"minutos",
            "segundos":"segundos",
            "milesimas":"milesimas",
            "Vasdelante":"Vas por delante",
            "Vasdetras":"Vas por detrás",
            "Hasganado":"¡Has ganado!",
            "Hasperdido":"¡Has perdido!"
        },



        "EN":{
            "ComienzaCarrera":"Race start!",
            "minutos":"minutes",
            "segundos":"seconds",
            "milesimas":"milliseconds",
            "Vasdelante":"You go ahead",
            "Vasdetras":"You go behind",
            "Hasganado":"You win!",
            "Hasperdido":"You lost!"
        }
    };

    static traducir(_st){
        let trad = new Traductor();
        if(!trad.traducciones[trad.language] || !trad.traducciones[trad.language][_st]){
            return "";
        }
        return trad.traducciones[trad.language][_st];
    }
}