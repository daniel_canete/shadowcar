import Tts from 'react-native-tts';

export default class Hablar{
    static frase(_str){
        Tts.speak(_str);
    }
}