
import {DeviceEventEmitter} from 'react-native';
import Utilidades from './Utilidades';
import Constantes from './Constantes';


export default class Redis {

    client;
    sombra = new Sombra();

    constructor(){
        this.reset();
    }

    reset(){
        var _nom = Utilidades.randomString(16, 'aA');
        this.sombra = new Sombra(_nom);
    }

    addLocation(_loc){
        this.sombra.addLocation(_loc);
    }

    getNombre(){
        return this.sombra.nombre;
    }

    getSombra(_key,_cb){

        var self=this;
        fetch(Constantes.URLApi+"get/"+_key, {
            headers: {
                Accept: 'application/json'
            }
        })
        .then((response) => response.json())
        .then((responseJson) => {
            _cb(responseJson);
        })
        .catch((error) => {
            self.handleError(error);
        });

    }

    sendSombra(){
        
        var self=this;
        fetch(Constantes.URLApi+"add", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.sombra)
        })
        .then((response) => response.json())
        .then((responseJson) => {
            self.reset();
            return responseJson;
        })
        .catch((error) => {
            self.handleError(error);
        });
    }

    handleError(error){
        console.error(error);
    }



}

class Location {
    longitude;
    latitude;
    millis;

    constructor(_lo,_la,_mi){
        this.longitude=_lo;
        this.latitude=_la;
        this.millis=_mi;
    }
}

class Sombra {
    nombre;
    fecha;
    locations;

    constructor(_n){
        this.nombre=_n;
        this.fecha=new Date();
        this.locations = [];
    }

    addLocation(_loc){
        this.locations.push(_loc);
    }
}



/*
{
	nombre:"sombraXX4",
	fecha:"2018-01-01 02:30",
	locations:[
		{
			longitude:-34.34534554,
			latitude:4.234234,
			millis:1234
			
		},
		{
			longitude:-35.34534554,
			latitude:5.234234,
			millis:12345
			
		}
	]
}

*/